package sd.lab.im;

import sd.lab.agency.AID;
import sd.lab.agency.behaviour.Behaviour;
import sd.lab.agency.impl.AbstractAgent;

import java.util.Objects;

import static sd.lab.agency.behaviour.Behaviour.*;

public class ChatterAgent extends AbstractAgent {

    private final String other;
    private AID otherAid;

    public ChatterAgent(String name, String other) {
        super(name);
        this.other = Objects.requireNonNull(other);
    }

    @Override
    public void setup() {
        otherAid = getEnvironment().aidOf(other);
        println("%s agent started.", getAID());
        addBehaviour(echoMyMessages());
        addBehaviour(listenOtherMessages());
    }

    private Behaviour echoMyMessages() {
        return readLine(System.in, this::onLineRead).repeatForEver();
    }

    private Behaviour listenOtherMessages() {
        return receiveAnyMessageFromAnyone(this::onMessageReceived).repeatForEver();
    }

    private void onLineRead(String line) {
        if (line.trim().startsWith(":end")) {
            addBehaviour(halt());
        } else {
            addBehaviour(send(otherAid, line));
        }
    }

    private void onMessageReceived(AID sender, String message) {
        println("[%s] %s", sender, message);
    }

    private Behaviour halt() {
        return Behaviour.of(() -> println("Halted.")).andThen(stopAgent());
    }

    private void println(String format, Object... objects) {
        System.out.println(String.format(format, objects));
    }
}
