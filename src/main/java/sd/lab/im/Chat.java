package sd.lab.im;

import sd.lab.agency.Environment;

import java.util.concurrent.ExecutionException;

public class Chat {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        var host = args[0];
        var port = Integer.parseInt(args[1]);
        var me = args[2];
        var other = args[3];
        var envName = Chat.class.getName();

        var environment = Environment.distributed(envName, host, port);

        environment.setLoggingEnabled(false);

        var agent = environment.createAgent(ChatterAgent.class, me, other);

        agent.start();

        agent.await();
    }
}
