package sd.lab.agency.impl;

import sd.lab.agency.fsm.impl.ThreadBasedAgentFSM;
import sd.lab.linda.textual.TextualSpace;

import java.util.Objects;

public class DistributedMultiThreadedEnvironment<A extends ThreadBasedAgentFSM> extends MultiThreadedEnvironment<A> {

    private final String hostname;
    private final int port;

    public DistributedMultiThreadedEnvironment(String name, String hostname, int port) {
        super(name);
        this.hostname = Objects.requireNonNull(hostname);
        this.port = port;
    }

    @Override
    protected TextualSpace newTextualSpace(String name) {
        return TextualSpace.remote(hostname, port, name);
    }
}
