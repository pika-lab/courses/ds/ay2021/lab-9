package sd.lab.agency;

import sd.lab.agency.fsm.AgentFSM;
import sd.lab.agency.fsm.impl.ExecutorBasedAgentFSM;
import sd.lab.agency.fsm.impl.ThreadBasedAgentFSM;
import sd.lab.agency.impl.DistributedMultiThreadedEnvironment;
import sd.lab.agency.impl.ExecutorBasedEnvironment;
import sd.lab.agency.impl.MultiThreadedEnvironment;
import sd.lab.linda.textual.TextualSpace;

import java.time.Duration;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeoutException;

public interface Environment<A extends AgentFSM> {

    boolean isLoggingEnabled();

    void setLoggingEnabled(boolean value);

    <B extends A> B createAgent(Class<B> clazz, String name, Object... args);

    <B extends A> B registerAgent(B agent);

    Set<AID> getAgents();

    String getName();

    TextualSpace getTextualSpace(String name);

    void awaitAllAgents(Duration duration) throws InterruptedException, ExecutionException, TimeoutException;

    AID aidOf(String localOrFullName);

    static <A extends ThreadBasedAgentFSM> Environment<A> multiThreaded(String name) {
        return new MultiThreadedEnvironment(name);
    }

    static <A extends ThreadBasedAgentFSM> Environment<A> multiThreaded() {
        return multiThreaded(null);
    }

    static <A extends ExecutorBasedAgentFSM> Environment<A> executorBased(String name, ExecutorService executorService) {
        return new ExecutorBasedEnvironment(name, executorService);
    }

    static <A extends ExecutorBasedAgentFSM> Environment<A> executorBased(String name) {
        return executorBased(name, Executors.newSingleThreadExecutor());
    }

    static <A extends ExecutorBasedAgentFSM> Environment<A> executorBased(ExecutorService executorService) {
        return executorBased(null, executorService);
    }

    static <A extends ExecutorBasedAgentFSM> Environment<A> executorBased() {
        return executorBased((String) null);
    }

    static <A extends ThreadBasedAgentFSM> Environment<A> distributed(String name, String host, int port) {
        return new DistributedMultiThreadedEnvironment<>(name, host, port);
    }

    static <A extends ThreadBasedAgentFSM> Environment<A> distributed(String host, int port) {
        return distributed(null, host, port);
    }
}
