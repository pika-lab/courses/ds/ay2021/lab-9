package sd.lab.agency.fsm.impl;

enum Operation {
    CONTINUE, RESTART, PAUSE, STOP
}
