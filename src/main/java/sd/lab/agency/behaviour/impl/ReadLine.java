package sd.lab.agency.behaviour.impl;

import sd.lab.agency.Agent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

public abstract class ReadLine extends AwaitPromise<String> {

    private final BufferedReader reader;

    public ReadLine(Reader reader) {
        this(new BufferedReader(Objects.requireNonNull(reader)));
    }

    private ReadLine(BufferedReader reader) {
        this.reader = Objects.requireNonNull(reader);
    }

    @Override
    public CompletableFuture<String> invokeAsync(Agent agent) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                return reader.readLine();
            } catch (IOException e) {
                return e;
            }
        }).thenCompose(it -> it instanceof String
                ? CompletableFuture.completedFuture((String) it)
                : CompletableFuture.failedFuture((IOException) it)
        );
    }
}
